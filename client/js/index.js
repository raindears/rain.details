define([], function() {

  /**
   * A reusable HTML5 details component with fallback that supports setting a summary and content. The details component
   * can either be opened or closed.
   *
   * Handlebar example for an open details component:
   * <code>{{component name="details" view="index" summary="My summary" content="<p>My content</p>" open=true sid="myDetails"}}</code>
   *
   * Example to use an existing DOM element with id="myNewContent" as content in your controller:
   * <code>details.setContent("#myNewContent", true);</code>
   *
   * @name Details
   * @constructor
   */
  function Details() {}

  Details.prototype = {

    openAttr: 'open',

    init: function() {},

    start: function() {
      this.$details = this.context.getRoot().find('details');
      this.$summary = $('summary', this.$details);
      this.$content = $('.details-content', this.$details);

      if (!(this.openAttr in this.$details[0])) {
        this.__implementDetailsElementBehaviour();
      }
    },

    /**
     * Implements the behaviour of the details element as it is not implemented
     * by all browsers at the moment.
     */
    __implementDetailsElementBehaviour: function() {
      this.$summary.click(this.toggleOpen.bind(this));
    },

    /**
     * Shorthand to configure the details element. If a "contentSelector" and a "content" is specified,
     * the "contentSelector" will be used.
     *
     * @param {Map} options The options ("summary", "content", "contentSelector", "open")
     */
    configure: function (options) {
      this.__throwIfNotStarted("configure");

      this.setSummary(options.summary);

      if (options.contentSelector !== undefined) {
        this.setContent(options.contentSelector, true);
      }
      else {
        this.setContent(options.content);
      }

      this.setOpen(options.open);

      return this;
    },

    /**
     * Sets the summary of the details element
     *
     * @param {String} summary The summary text
     */
    setSummary: function(summary) {
      this.__throwIfNotStarted("setSummary");

      if (summary || summary === "") {
        this.$summary.html(summary);
      }
    },

    /**
     * Sets the content of the details element
     *
     * @param {String} content The HTML content or a jQuery selector
     * @param {Boolean} isSelector Whether the content is a jQuery selector
     */
    setContent: function(content, isSelector) {
      this.__throwIfNotStarted("setContent");

      if (content || content === "") {
        if (isSelector) {
          content = $(content);
        }
        this.$content.html(content);
      }

    },

    /**
     * Sets the details element open or not
     *
     * @param open {Boolean} open Whether to display opened or not
     */
    setOpen: function(open) {
      this.__throwIfNotStarted("setOpen");

      var openAttr = this.openAttr;
      var detailsElem = this.$details;

      if (open) {
        detailsElem.attr(openAttr, openAttr);
      }
      else {
        detailsElem.removeAttr(openAttr);
      }
    },

    /**
     * Toggles the open state of the details element
     */
    toggleOpen: function() {
      this.__throwIfNotStarted("toggleOpen");

      this.setOpen(!this.isOpen());
    },

    /**
     * Returns whether the details element is open or not
     *
     * @return {Boolean} Whether the details element is open or not
     */
    isOpen: function() {
      this.__throwIfNotStarted("isOpen");

      return (this.$details.attr(this.openAttr) === this.openAttr);
    },

    __throwIfNotStarted: function(methodName) {
      if (!this.$details) {
        throw new RainError(methodName + '() cannot be called before start is executed',
          RainError.ERROR_PRECONDITION_FAILED);
      }
    }
  };

  return Details;

});
